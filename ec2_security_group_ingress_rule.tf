resource "aws_security_group_rule" "ingress_rule" {
  security_group_id = var.target_security_group_id
  type              = "ingress"

  from_port                = var.from_port
  to_port                  = var.to_port
  protocol                 = var.protocol
  source_security_group_id = var.inboard_source_security_group_id
  description              = var.description
}

