#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "account_id" {
  type    = string
  default = ""
}

#-----------------------------------------------------------------------------------------------------
# INPUT
#-----------------------------------------------------------------------------------------------------
variable "target_security_group_id" {
  type = string
}

variable "inboard_source_security_group_id" {
  type = string
}

variable "description" {
  type = string
}

variable "from_port" {
  type = number
}

variable "to_port" {
  type = number
}

variable "protocol" {
  type = string
}
